package com.company.myskyroof.service;

import com.company.myskyroof.dao.PermissionRepository;
import com.company.myskyroof.dao.ProjectRepository;
import com.company.myskyroof.dao.UserRepository;
import com.company.myskyroof.dto.ProjectForUser;
import com.company.myskyroof.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    public Collection<ProjectForUser> findProjectsForUser(String userName) {
        User user = userRepository.findByUsername(userName);
//
//        return projectRepository.findByUserName(user.getUserID());
        return projectRepository.findProjectPermissionByUser(user.getUserID()).stream()
                .map((per) -> new ProjectForUser(per.getProject().getProjectID(), per.getProject().getName(), per.getPermissiondescr()))
                .collect(Collectors.toSet());
    }
}
