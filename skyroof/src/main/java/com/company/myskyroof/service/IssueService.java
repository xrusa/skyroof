package com.company.myskyroof.service;

import com.company.myskyroof.dao.*;
import com.company.myskyroof.dto.IssueCreateRequest;
import com.company.myskyroof.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class IssueService {

    public static final String PARAM_TITLE = "title";
    public static final String PARAM_PROJECT_ID = "projectId";
    public static final String PARAM_STATUS_ID = "statusId";
    public static final String PARAM_ASSIGNOR_ID = "assignorId";
    public static final String PARAM_ASSIGNEE_ID = "assigneeId";
    public static final String PARAM_ISSUE_TYPE = "issueType";
    public static final String PARAM_ALL_OPEN_ISSUES = "allOpenIssues";
    public static final String PARAM_ALL_CLOSED_ISSUES = "allClosedIssues";

    public static final List<String> issueTypes = Arrays.asList("error", "improvement", "other");

    public static final String VAL_ISSUE_OPEN = "OPEN";
    public static final String VAL_ISSUE_CLOSED = "CLOSED";

    public static final String PERMISSION_VAL_WRITE = "WRITE";
    public static final String PERMISSION_VAL_DELETE = "DELETE";

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private StatusRepository statusRepository;

    private void checkPermissions(Integer userId, Integer projectId, String permissionType) {
        Permission permission = permissionRepository.findByUserIdAndProjectId(userId, projectId);

        if (permission == null) {
            throw new RuntimeException("No Permission found for user with id: " + userId);
        }

        if (!permission.getPermissiondescr().equalsIgnoreCase(permissionType)) {
            throw new RuntimeException(permissionType + " permission not supported for user id : " + userId + " and project id: " + projectId);
        }
    }

    //ISSUE RETRIEVAL
    // METHOD: GET
    // URL: /issues?title={title}&
    // RESPONSE: {}

    // ISSUE CREATION
    // METHOD: POST
    // URL: /issues Creation of issue
    // BODY:
    // RESPONSE

    // ISSUE UPDATE
    // METHOD: PUT
    // URL: /issues/{issueId}
    // RESPONSE: {

    @Transactional
    public Issue save(IssueCreateRequest request, String currentUserName, Integer updateId) {
        User assignor = userRepository.findByUsername(currentUserName);

        Project project = projectRepository.findById(request.getProjectId())
                .orElseThrow(() -> new RuntimeException("Could not find project with id: " + request.getProjectId()));

        User assignee = userRepository.findById(request.getAssigneeId())
                .orElseThrow(() -> new RuntimeException("Could not find user with id: " + request.getAssigneeId()));

        Status status = statusRepository.findById(request.getStatusId())
                .orElseThrow(() -> new RuntimeException("Could not find status with id: " + request.getStatusId()));

        //check if current user has write persmissions
        checkPermissions(assignor.getUserID(), project.getProjectID(), IssueService.PERMISSION_VAL_WRITE);

        Issue issue;
        //if issue exists then update
        if (updateId != null) {
            issue = issueRepository.findById(updateId)
                    .orElseThrow(() -> new RuntimeException("Could not find issue with id: " + updateId));
        } else {
            issue = new Issue();
        }

        //map dto to entity
        issue.setProject(project);
        issue.setAssignor(assignor);
        issue.setStatus(status);
        issue.setAssignee(assignee);
        issue.setDescription(request.getDescription());
        issue.setTitle(request.getTitle());
        issue.setOtherdetails(request.getOtherDetails());
        issue.setTypeissue(request.getType());
        return issueRepository.save(issue);
    }

    public void delete(Integer issueId, String currentUserName) {
        User assignor = userRepository.findByUsername(currentUserName);

        Issue issue = issueRepository.findById(issueId)
                .orElseThrow(() -> new RuntimeException("Could not find issue with id: " + issueId));
        checkPermissions(assignor.getUserID(), issue.getProject().getProjectID(), IssueService.PERMISSION_VAL_DELETE);
//check closed
        if (issue.getStatus().getDescription().equalsIgnoreCase(VAL_ISSUE_CLOSED)) {
            throw new RuntimeException("Cannot delete issue with id " + issueId + " as it has been closed");
        }
        issueRepository.delete(issue);
    }

    public List<Issue> filter(MultiValueMap<String, String> params) {
        final String title = params.getFirst(PARAM_TITLE);

        final Integer projectId = Optional.ofNullable(params.getFirst(PARAM_PROJECT_ID))
                .map(Integer::parseInt)
                .orElse(null);

        final List<Integer> assignorIds = Optional.ofNullable(params.get(PARAM_ASSIGNOR_ID))
                .map(p -> p.stream().map(Integer::parseInt).collect(Collectors.toList()))
                .orElse(new ArrayList<>());

        final Integer assigneeId = Optional.ofNullable(params.getFirst(PARAM_ASSIGNEE_ID))
                .map(Integer::parseInt)
                .orElse(null);

        final Integer statusId = Optional.ofNullable(params.getFirst(PARAM_STATUS_ID))
                .map(Integer::parseInt)
                .orElse(null);

        final String issueType = params.getFirst(PARAM_ISSUE_TYPE);


        final Boolean allOpenIssues = Optional.ofNullable(params.getFirst(PARAM_ALL_OPEN_ISSUES))
                .map(Boolean::parseBoolean)
                .orElse(null);


        final Boolean allClosedIssues = Optional.ofNullable(params.getFirst(PARAM_ALL_CLOSED_ISSUES))
                .map(Boolean::parseBoolean)
                .orElse(null);


        List<Issue> issues = issueRepository.findAll().stream()
                .filter((i) -> title == null || i.getTitle().contains(title))
                .filter((i) -> projectId == null || i.getProject().getProjectID().equals(projectId))
                .filter((i) -> statusId == null || i.getStatus().getStatusID().equals(statusId))
                .filter((i) -> assignorIds.isEmpty() || assignorIds.contains(i.getAssignor().getUserID()))
                .filter((i) -> assigneeId == null || i.getAssignee().getUserID().equals(assigneeId))
                .filter((i) -> issueType == null || issueTypes.contains(i.getTypeissue().toLowerCase()))
                .filter((i) -> allOpenIssues == null || i.getStatus().getDescription().equalsIgnoreCase(VAL_ISSUE_OPEN))
                .filter((i) -> allClosedIssues == null || i.getStatus().getDescription().equalsIgnoreCase(VAL_ISSUE_CLOSED))
                .collect(Collectors.toList());

        return issues;

    }
}
