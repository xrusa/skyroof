/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.myskyroof.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author darra
 */
@Entity
@Table(name = "project")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projectID")
    private Integer projectID;

    @Lob
    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    private Collection<Issue> issueCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    private Collection<Permission> permissionCollection;

    public Project() {
    }

    public Project(Integer projectID) {
        this.projectID = projectID;
    }

    public Project(Integer projectID, String name) {
        this.projectID = projectID;
        this.name = name;
    }

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Collection<Issue> getIssueCollection() {
        return issueCollection;
    }

    public void setIssueCollection(Collection<Issue> issueCollection) {
        this.issueCollection = issueCollection;
    }

    @JsonIgnore
    public Collection<Permission> getPermissionCollection() {
        return permissionCollection;
    }

    public void setPermissionCollection(Collection<Permission> permissionCollection) {
        this.permissionCollection = permissionCollection;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Project)) {
            return false;
        }
        Project project = (Project) o;
        return Objects.equals(projectID, project.projectID) &&
               Objects.equals(name, project.name) &&
               Objects.equals(issueCollection, project.issueCollection) &&
               Objects.equals(permissionCollection, project.permissionCollection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectID, name, issueCollection, permissionCollection);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Project{");
        sb.append("projectID=").append(projectID);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
