/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.myskyroof.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author darra
 */
@Entity
@Table(name = "permission")
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PermissionID")
    private Integer permissionID;

    @Column(name = "Permissiondescr")
    private String permissiondescr;

    @JoinColumn(name = "projectID", referencedColumnName = "projectID")
    @ManyToOne(optional = false)
    private Project project;

    @JoinColumn(name = "userID", referencedColumnName = "userID")
    @ManyToOne(optional = false)
    private User user;

    public Permission() {
    }

    public Permission(Integer permissionID) {
        this.permissionID = permissionID;
    }

    public Permission(Integer permissionID, String permissiondescr) {
        this.permissionID = permissionID;
        this.permissiondescr = permissiondescr;
    }

    public Integer getPermissionID() {
        return permissionID;
    }

    public void setPermissionID(Integer permissionID) {
        this.permissionID = permissionID;
    }

    public String getPermissiondescr() {
        return permissiondescr;
    }

    public void setPermissiondescr(String permissiondescr) {
        this.permissiondescr = permissiondescr;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project projectID) {
        this.project = projectID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User userID) {
        this.user = userID;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Permission)) {
            return false;
        }
        Permission that = (Permission) o;
        return Objects.equals(permissionID, that.permissionID) &&
               Objects.equals(permissiondescr, that.permissiondescr) &&
               Objects.equals(project, that.project) &&
               Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(permissionID, permissiondescr, project, user);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Permission{");
        sb.append("permissionID=").append(permissionID);
        sb.append(", permissiondescr='").append(permissiondescr).append('\'');
        sb.append(", project=").append(project);
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
