/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.myskyroof.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author darra
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userID")
    private Integer userID;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assignor")
    private Collection<Issue> assignors;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assignee")
    private Collection<Issue> assignees;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Collection<Permission> permissionCollection;

    public User() {
    }

    public User(Integer userID) {
        this.userID = userID;
    }

    public User(Integer userID, String username, String password, String email) {
        this.userID = userID;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public Collection<Issue> getAssignors() {
        return assignors;
    }

    public void setAssignors(Collection<Issue> issueCollection) {
        this.assignors = issueCollection;
    }

    @JsonIgnore
    public Collection<Issue> getAssignees() {
        return assignees;
    }

    public void setAssignees(Collection<Issue> issueCollection1) {
        this.assignees = issueCollection1;
    }

    @JsonIgnore
    public Collection<Permission> getPermissionCollection() {
        return permissionCollection;
    }

    public void setPermissionCollection(Collection<Permission> permissionCollection) {
        this.permissionCollection = permissionCollection;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(userID, user.userID) &&
               Objects.equals(username, user.username) &&
               Objects.equals(password, user.password) &&
               Objects.equals(email, user.email) &&
               Objects.equals(assignors, user.assignors) &&
               Objects.equals(assignees, user.assignees) &&
               Objects.equals(permissionCollection, user.permissionCollection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userID, username, password, email, assignors, assignees, permissionCollection);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("userID=").append(userID);
        sb.append(", username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", assignors=").append(assignors);
        sb.append(", assignees=").append(assignees);
        sb.append(", permissionCollection=").append(permissionCollection);
        sb.append('}');
        return sb.toString();
    }
}
