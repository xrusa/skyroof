/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.myskyroof.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author darra
 */
@Entity
@Table(name = "issue")
public class Issue implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "issueID")
    private Integer issueID;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "typeissue")
    private String typeissue;

    @Column(name = "otherdetails")
    private String otherdetails;

    @JoinColumn(name = "projectID", referencedColumnName = "projectID")
    @ManyToOne(optional = false)
    private Project project;

    @JoinColumn(name = "statusID", referencedColumnName = "statusID")
    @ManyToOne(optional = false)
    private Status status;

    @JoinColumn(name = "assignor", referencedColumnName = "userID")
    @ManyToOne(optional = false)
    private User assignor;

    @JoinColumn(name = "assignee", referencedColumnName = "userID")
    @ManyToOne(optional = false)
    private User assignee;

    public Issue() {
    }

    public Issue(Integer issueID) {
        this.issueID = issueID;
    }

    public Issue(Integer issueID, String title, String description, String typeIssue, String otherDetails) {
        this.issueID = issueID;
        this.title = title;
        this.description = description;
        this.typeissue = typeIssue;
        this.otherdetails = otherDetails;
    }

    public Integer getIssueID() {
        return issueID;
    }

    public void setIssueID(Integer issueID) {
        this.issueID = issueID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypeissue() {
        return typeissue;
    }

    public void setTypeissue(String typeissue) {
        this.typeissue = typeissue;
    }

    public String getOtherdetails() {
        return otherdetails;
    }

    public void setOtherdetails(String otherdetails) {
        this.otherdetails = otherdetails;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project projectID) {
        this.project = projectID;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status statusID) {
        this.status = statusID;
    }

    public User getAssignor() {
        return assignor;
    }

    public void setAssignor(User assignor) {
        this.assignor = assignor;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Issue)) {
            return false;
        }
        Issue issue = (Issue) o;
        return Objects.equals(issueID, issue.issueID) &&
               Objects.equals(title, issue.title) &&
               Objects.equals(description, issue.description) &&
               Objects.equals(typeissue, issue.typeissue) &&
               Objects.equals(otherdetails, issue.otherdetails) &&
               Objects.equals(project, issue.project) &&
               Objects.equals(status, issue.status) &&
               Objects.equals(assignor, issue.assignor) &&
               Objects.equals(assignee, issue.assignee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(issueID, title, description, typeissue, otherdetails, project, status, assignor, assignee);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Issue{");
        sb.append("issueID=").append(issueID);
        sb.append(", title='").append(title).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", typeissue='").append(typeissue).append('\'');
        sb.append(", otherdetails='").append(otherdetails).append('\'');
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
