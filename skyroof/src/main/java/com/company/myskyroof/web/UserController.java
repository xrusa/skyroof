package com.company.myskyroof.web;

import com.company.myskyroof.dao.PermissionRepository;
import com.company.myskyroof.dao.ProjectRepository;
import com.company.myskyroof.dao.UserRepository;
import com.company.myskyroof.dto.IssueCreateRequest;
import com.company.myskyroof.model.Issue;
import com.company.myskyroof.model.Permission;
import com.company.myskyroof.model.Project;
import com.company.myskyroof.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private ProjectRepository projectRepository;

    @ResponseBody
    @GetMapping("/users")
    public List<User> getAllPersons() {

        List<User> all = userRepository.findAll();
        return all;
    }

// @RequestMapping("/permissionissue")
//   public List<Permission> findPermissions(@AuthenticationPrincipal UserDetails userDetails){
//        System.out.println (userDetails.getUsername());
//        userRepository.findByUsername(userDetails.getUsername());
////        System.out.println (userRepository.findByUsername(userDetails.getUsername()));
//        Integer userID_temp = (userRepository.findByUsername(userDetails.getUsername())).getUserID();
//        System.out.println(userID_temp);
//        List<Permission> permissions = permissionRepository.findByUserID(userID_temp);
//        //List<Permission> permissions = permissionRepository.findAll();
//
//        return permissions;


//    @RequestMapping("/permissionissue")
//    public List<Permission> findPermissions(@AuthenticationPrincipal UserDetails userDetails){
//        User loggedUser = userRepository.findByUsername(userDetails.getUsername());
//        List<Permission> permissions = permissionRepository.findAllByUserID(loggedUser.getUserID());
//        return permissions;

//        List<Project> projects = new ArrayList();
//        for (int i = 0; i < permissions.size(); i++){
//            Project project = projectRepository.findByProjectID(permissions.get(i).getProjectID());
//            projects.add(project);
//
//        return projects;



//    @ResponseBody
//    @ExceptionHandler
//    @PostMapping("/addnewuser")
//    public User addnewperson(@RequestBody User user) {
//       System.out.println(user.toString());
//       User save = userRepository.save(user);
//       return save;
//
//    }
}

