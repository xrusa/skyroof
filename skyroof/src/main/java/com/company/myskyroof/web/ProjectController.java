package com.company.myskyroof.web;


import com.company.myskyroof.dao.ProjectRepository;
import com.company.myskyroof.dto.ProjectForUser;
import com.company.myskyroof.model.Project;
import com.company.myskyroof.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectService projectService;

    @ResponseBody
    @GetMapping("/projectsper")
    public ResponseEntity<Collection<ProjectForUser>> getAllProjects(
            @AuthenticationPrincipal UserDetails userDetails) {
        Collection<ProjectForUser> all = projectService.findProjectsForUser(userDetails.getUsername());
        return ResponseEntity.ok(all);
    }

    @ResponseBody
    @GetMapping("/projects")
    public List<Project> getAllProjects() {

        List<Project> all = projectRepository.findAll();
        return all;
    }

    @ResponseBody
    @ExceptionHandler
    @PostMapping("/addnewproject")
    public Project addNewProjects(@RequestBody Project project){
        System.out.println(project.toString());
        Project save = projectRepository.save(project);
        return save;

    }
    @ResponseBody
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProject(@PathVariable("id") Integer projectId, @AuthenticationPrincipal UserDetails userDetails) {
        projectRepository.deleteById(projectId);
        return ResponseEntity.noContent().build();
    }
}

