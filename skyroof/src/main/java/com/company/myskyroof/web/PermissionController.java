package com.company.myskyroof.web;
import com.company.myskyroof.dao.PermissionRepository;
import com.company.myskyroof.model.Permission;
import com.company.myskyroof.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/permissions")
public class PermissionController {

    @Autowired
    private PermissionRepository permissionRepository;

    @ResponseBody
    @GetMapping
    public List<Permission> getAllPermissions() {

        List<Permission> all = permissionRepository.findAll();
        return all;
    }
    @ResponseBody
    @ExceptionHandler
    @PostMapping
    public Permission addnewpermissions(@RequestBody Permission permission){
        System.out.println(permission.toString());
        Permission save = permissionRepository.save(permission);
        return save;

    }



}

