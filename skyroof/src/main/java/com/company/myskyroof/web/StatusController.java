package com.company.myskyroof.web;

import com.company.myskyroof.dao.StatusRepository;
import com.company.myskyroof.model.Status;
import javassist.runtime.Desc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StatusController {

    @Autowired
    private StatusRepository statusRepository;

    @ResponseBody
    @GetMapping("/status")
    public List<Status> getAllStatus() {

        List<Status> all = statusRepository.findAll();
        return all;
    }
    @ResponseBody
    @ExceptionHandler
    @PostMapping("/addnewstatus")
    public Status addnewstatus(@RequestBody Status status){

        System.out.println(status.toString());
        Status save = statusRepository.save(status);
        return save;

    }
//    @ResponseBody
//    @GetMapping("/laststatusid")
//    public List<Status> getLastStatus() {
//
//        List<Status> all = statusRepository.findOne(status)
//        return all;
//    }
}
