package com.company.myskyroof.web;

import com.company.myskyroof.dto.IssueCreateRequest;
import com.company.myskyroof.model.Issue;
import com.company.myskyroof.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/issues")
public class IssueController {

    @Autowired
    private IssueService issueService;

    @ResponseBody
    @GetMapping //localhost:8080/issues?title=aaa&projectId=1&assignor=5&assignee=4&status=string&issueType=string
    public ResponseEntity<List<Issue>> getAllIssues(@RequestParam MultiValueMap<String, String> params) {


        List<Issue> all = issueService.filter(params);
        return ResponseEntity.ok(all);
    }

    @ResponseBody
    @PostMapping //localhost:8080/issues
    public ResponseEntity<Issue> addNewIssue(@RequestBody IssueCreateRequest issue, @AuthenticationPrincipal UserDetails userDetails){
        Issue rtn = issueService.save(issue, userDetails.getUsername(), null);
        return ResponseEntity.ok(rtn);
    }

    @ResponseBody
    @PutMapping("/{id}") //localhost:8080/issues/{id}
    public ResponseEntity<Issue> updateIssue(@PathVariable("id") Integer issueId, @RequestBody IssueCreateRequest issue, @AuthenticationPrincipal UserDetails userDetails) {
        Issue rtn = issueService.save(issue, userDetails.getUsername(), issueId);
        return ResponseEntity.ok(rtn);
    }

    @ResponseBody
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteIssue(@PathVariable("id") Integer issueId, @AuthenticationPrincipal UserDetails userDetails) {
        issueService.delete(issueId, userDetails.getUsername());
        return ResponseEntity.noContent().build();
    }
}



