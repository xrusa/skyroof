package com.company.myskyroof.dto;

public class ProjectForUser {

    private Integer projectId;

    private String projectName;

    private String permissionDescription;

    public ProjectForUser() {

    }

    public ProjectForUser(Integer projectId, String projectName, String permissionDescription) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.permissionDescription = permissionDescription;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPermissionDescription() {
        return permissionDescription;
    }

    public void setPermissionDescription(String permissionDescription) {
        this.permissionDescription = permissionDescription;
    }
}
