package com.company.myskyroof.dao;

import com.company.myskyroof.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {
    @Override
    <S extends Status> S save(S s);

}

