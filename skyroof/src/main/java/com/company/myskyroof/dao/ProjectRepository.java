package com.company.myskyroof.dao;

import com.company.myskyroof.dto.ProjectForUser;
import com.company.myskyroof.model.Permission;
import com.company.myskyroof.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    @Override
    <S extends Project> S save(S s);

    List<Project> findByProjectID(Integer projectId);

//    @Query("select new com.company.myskyroof.dto.ProjectForUser(p.projectID, p.name, per.permissiondescr) from Project p " +
//            "join p.permissionCollection per " +
//            "where per.user.userID = :userId"
//    )
//    Set<ProjectForUser> findByUserName(Integer userId);

    @Query("select per from Permission per where per.user.userID = :userId")
    Set<Permission> findProjectPermissionByUser(Integer userId);

}
