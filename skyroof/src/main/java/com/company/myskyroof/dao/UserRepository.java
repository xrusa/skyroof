package com.company.myskyroof.dao;


import com.company.myskyroof.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Override
    <S extends User> S save(S s);

    User findByUsername(String username);


    //GET localhost:8080/users?&password=3434&password=5&username=blabla
//    @Query("select u from UserEntity u where (password = null || password = ?3) ")
//    List<UserEntity> filter(Long id, String password, String username, String email);
}