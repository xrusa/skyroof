package com.company.myskyroof.dao;
import com.company.myskyroof.model.Issue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Integer> {

    @Override
    <S extends Issue> S save(S s);

//    @Query()
//    List<Issue> filter(
//            String title, Integer projectId, Integer assignorId, Integer assigneeId, String status, String issueType);
}



