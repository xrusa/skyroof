package com.company.myskyroof.dao;
import com.company.myskyroof.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface PermissionRepository extends JpaRepository<Permission, Integer> {

    @Override
    <S extends Permission> S save(S s);

    @Query("select p from Permission p where p.user.userID = :userId and p.project.projectID = :projectId")
    Permission findByUserIdAndProjectId(Integer userId, Integer projectId);

//    @Query("select p from Permission p where p.user.userID = :userId")
//    List<Permission> findByUserID(Integer userId);


}
