package com.company.myskyroof.searchcriteria;

import com.company.myskyroof.model.Issue;
import com.company.myskyroof.model.Project;
import com.company.myskyroof.model.Status;
import com.company.myskyroof.model.User;

public class SearchQueries {

    private String title;
    private Project project;
    private User assignor;
    private User assignee;
    private Status status;
    private Issue typeissue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getAssignor() {
        return assignor;
    }

    public void setAssignor(User assignor) {
        this.assignor = assignor;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Issue getTypeissue() {
        return typeissue;
    }

    public void setTypeissue(Issue typeissue) {
        this.typeissue = typeissue;
    }

    public SearchQueries(String title, Project project, User assignor, User assignee, Status status, Issue typeissue) {
        this.title = title;
        this.project = project;
        this.assignor = assignor;
        this.assignee = assignee;
        this.status = status;
        this.typeissue = typeissue;


    }
}
