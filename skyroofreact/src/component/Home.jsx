import React, { Component } from 'react'
import IssueDataService from '../service/IssuesService.js';
import ProjectsDataService from '../service/ProjectsService.js';
import UsersDataService from '../service/UsersService.js';
import Header1 from '../Header1';
import AuthenticationService from '../service/AuthenticationService.js';
import  '../index.css';

import { Col,Button, ButtonGroup, ButtonToolbar, Dropdown,DropdownButton,Nav,Navbar,Form,FormControl, Container} from 'react-bootstrap';

export var queryString;

// let token = sessionStorage.getItem('token')
// let user = sessionStorage.getItem('username')


function myFunction() {

   document.getElementById('formdisabled').style.display = "block";
}

function myFunction2() {

    document.getElementById('formdisabled').style.display = "none";
 }



const USERNAME = 'username'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            issues: [],
            message: null,
            projects: [],
            users:[]
            
        }
        
    }
    c

    componentDidMount() {
        this.refreshProjects();
        this.refreshUsers();
        this.refreshIssues();
       
   

    }
    


    refreshProjects() {
        ProjectsDataService.retrieveAllProjects(USERNAME)//HARDCODED
            .then(
                response => {
                    this.setState({ projects: response.data })
                    console.log({ projects: response.data })
                }
            )
    
    }

    refreshUsers() {
        UsersDataService.retrieveAllUsers(USERNAME)//HARDCODED
            .then(
                response => {
                  console.log(response.data);
                    this.setState({ users: response.data })
                }
            )
      }
      
      refreshIssues() {
        IssueDataService.retrieveAllIssues(USERNAME,queryString)//HARDCODED
            .then(
                response => {
                    console.log(response.data);
                    this.setState({ issues: response.data })
                }
            )
    
    }

    
    
      handle3=(e)=>{
        // alert('dd');
        e.preventDefault();
        const form = e.target;
       
        const sendtext100 =document.getElementById("prj1").value/1;
        const sendtext =document.getElementById("prjtitle").value;
        const sendtext1 =document.getElementById("status1").value;
        const sendtext2 =document.getElementById("typeissue").value;
        const sendtext3 =document.getElementById("otherid").value/1;
    
        const textjs={
        
        "projectId":sendtext100,
        "title":sendtext,
        "status":sendtext1,
        "issueType": sendtext2,
        "assignor": sendtext3
       
      };

      queryString = Object.keys(textjs).map(key => key + '=' + textjs[key]).join('&');
      console.log(queryString);
      this.refreshIssues(queryString);

      
    //   fetch("http://localhost:8080/issues?"+ queryString,
    //   {
    //       method: "GET",
    //       cache: "no-cache",
    //       headers:{
    //           "Content-Type": "application/json",
    //           "Authorization": "Bearer "+sessionStorage.getItem('token')
    //       },
          
    //   })
    //   .then(
    //     response => {
    //       console.log(response.data);
    //         this.setState({ issues: response.data })
    //     }
    // )

    
 }
 

    render() {
        const { projects } = this.state;
        const { users } = this.state;
        const { issues } = this.state;
        // console.log('render')
         console.log(this.state.issues);
        // console.log((this.state.issues).filter(issue => issue.assignor.username === USERNAME));

        return (
        <>
        <Container>
            <div className="navbar1">
                <Navbar bg="dark" variant="dark">
                    <Nav className="mr-auto">
                        <Nav.Link href="/CreatePage">&nbsp;Create issue &nbsp;&nbsp;</Nav.Link>
                        <Nav.Link href="#features">Search&nbsp;<span class="glyphicon glyphicon-search"></span></Nav.Link>
                        </Nav>
                        <Form inline>
                        <Button variant="outline-info"style={{ fontSize:'12pt'}} ><Nav.Link href="/logout" onClick={AuthenticationService.logout}>Logout &nbsp; <span class="glyphicon glyphicon-log-out"></span></Nav.Link></Button>
                        </Form>
                            </Navbar>
                            </div>
        </Container>
            <div className="Container">
                <Header1/>
                <div className="cont1">
                    
            
            <Form onSubmit={this.handle3}>
                <div class="row">
                <div className="col-md-3">
            <Form.Group controlId="formProjects"> 
                    <Form.Label>Projects</Form.Label>
                    {/* <Form.Control as="select">  */}
                    
                    <select onChange={this.handleChange}style={{ width: '100%',height:'30px', fontSize:'10pt' }}
                     id="prj1">
                    <option value="n" >Select project</option><hr></hr>
                    {this.state.projects.map(
                        project => 
                        
                         <option value={project.projectID}>
                            {project.name}
                                </option>
                                )}
                                </select> 
                   
                    {/* </Form.Control>  */}
                    </Form.Group>
                    </div>
                    <div className="col-md-3">
                    <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Project Title</Form.Label>
                    <Form.Control type="text" placeholder="Enter project title" id="prjtitle"/>
                    </Form.Group> 
                    </div>
                    <div className="col-md-3">
                    <Form.Group >
                    <Form.Label>Status</Form.Label>
                    {/* <Form.Control as="select"  style={{ width: '100%',height:'30px', fontSize:'10pt' }}> */}
                    <select style={{ width: '100%',height:'30px', fontSize:'10pt' }} id="status1">
                    <option value="">Enter project status</option>
                    <option value="1">OPEN</option>
                    <option value="2">RESOLVED</option>
                    <option value="3">REOPEN</option>
                    <option value="4">CLOSED</option>
                    {/* </Form.Control> */}
                    </select>
                    </Form.Group>
                    </div>
                    <div className="col-md-3">
                    <Form.Group >
                    <Form.Label>Type</Form.Label>
                    {/* <Form.Control id="type" required  style={{ width: '100%',height:'40px', fontSize:'13pt' }}> */}
                    <select style={{ width: '100%',height:'30px', fontSize:'10pt' }} id="typeissue">
                    <option value="">Enter project type</option>
                    <option>ERROR</option>
                    <option>IMPROVEMENT</option>
                    <option>OTHER</option>
                    </select>
                    {/* </Form.Control> */}
                    
                    </Form.Group>
                    </div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">
                    <Form.Label>Assignor</Form.Label> 
                    <Form.Check  style={{ fontSize:'9pt' }}
                    type="radio"
                    onClick={() => {myFunction2()}}
                    label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;me"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                    />
                   <Form.Check style={{ fontSize:'9pt' }}
                    type="radio"
                    label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;other"
                    onClick={() => {myFunction()}}
                    name="formHorizontalRadios"
                    
                    />
                    
                    </div>
                    <div className="col-md-6">
                    <Form.Group id="formdisabled"> 
                    <Form.Label>List of assignors</Form.Label>
                    {/* <Form.Control as="select"> */}
                    <select style={{ width: '100%',height:'30px', fontSize:'10pt' }} id="otherid">
                    {this.state.users.map(
                    user => 
                    <option value={user.userID}>
                    {user.username} 
                     </option>
                   )}
                   </select>
                    {/* </Form.Control> */}
                    </Form.Group>
                    </div>
                    </div>
                    <Container>
                        <hr></hr>
                    <div className="col-md-2">
                    </div>
                    <div className="col-md-2">
                    <center><Button variant="primary" type="submit" size="sm" style={{ fontSize:'10pt',width:'100px',backgroundColor:' #000033',opacity:'0.6',border:'1px solid #00001a',borderRadius:'5px'}}>Search</Button></center> 
                    </div>
                    <div className="col-md-2">
                    <center><Button variant="primary" size="sm" onClick={() => window.location.reload(false)} style={{ fontSize:'10pt',width:'100px',backgroundColor:' #000033',opacity:'0.6',border:'1px solid #00001a',borderRadius:'5px'}}>Clear</Button></center> 
                    </div>
                    <div className="col-md-2">
                    <center><Button variant="primary" size="sm" style={{ fontSize:'10pt',width:'120px',backgroundColor:' #000033',opacity:'0.6',border:'1px solid #00001a',borderRadius:'5px'}}>My Open Issues</Button></center> 
                    </div>
                    <div className="col-md-2">
                    <center><Button variant="primary" size="sm" style={{ fontSize:'10pt',width:'120px',backgroundColor:' #000033',opacity:'0.6',border:'1px solid #00001a',borderRadius:'5px'}}>All Open Issues</Button></center> 
                    </div>
                    <div className="col-md-2">
                    </div>
                    </Container>
                    <hr></hr>
                    </Form>
                  <Container> 
                <h3 style={{fontSize:'20pt',textAlign:'center',border:'1px solid #00001a',borderRadius:'5px', backgroundColor:' #e6e6ff',opacity:'0.6',color:'black'}}>Issues</h3>
                <div className="container" style={{ background: '#fffc',borderRadius: '5px'}}>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Issue Type</th>
                                <th>Project Name</th>
                                <th>Status</th>
                                <th>Assignor</th>
                                <th>Assignee</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                
                                this.state.issues.map(
                                    issue =>
                                        <tr key={issue.issueID}>
                                            <td>{issue.project.name}</td>
                                            <td>{issue.title}</td>
                                            <td>{issue.assignee.username}</td>
                                            <td>{issue.status.description}</td>
                                            <td>{issue.typeissue}</td>
                                            <td>{issue.assignor.username}</td>
                                        </tr>
                                )
                            }
                        </tbody>


                    </table>
                    </div>
                
                    </Container> 
            
        
            
        
            </div>
                </div>
                

                </>
        );
    }
}

export default Home