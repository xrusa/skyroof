import React, { Component } from 'react';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginComponent from './LoginComponent';
import LogoutComponent from './LogoutComponent';
import MenuComponent from './MenuComponent';
import AuthenticationService from '../service/AuthenticationService';
import AuthenticatedRoute from './AuthenticatedRoute';
import CreatePage from './CreatePage';


class UserApp extends Component {


    render() {
        return (
            <>
                <Router>
                    <>
                        
                        <Switch>
                            <Route path="/" exact component={LoginComponent} />
                            <Route path="/login" exact component={LoginComponent} />
                            <Route path="/home" exact component={Home} />
                            <Route path="/createpage" exact component={CreatePage} />
                            <AuthenticatedRoute path="/logout" exact component={LogoutComponent} />
                            
                        </Switch>
                    </>
                </Router>
            </>
        )
    }
}

export default UserApp