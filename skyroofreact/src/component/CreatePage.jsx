import React, { Component } from 'react'
import PermissionDataService from '../service/PermissionService.js';
import UsersDataService from '../service/UsersService.js';
import Header1 from '../Header1';
import  '../index.css';
import MenuComponent from './MenuComponent';
// import { Form,Button, FormGroup, FormControl, ControlLabel,Col } from "react-bootstrap";
import axios from 'axios';
import AuthenticatedRoute from './AuthenticatedRoute.jsx';
import AuthenticationService from '../service/AuthenticationService.js';
import IssueDataService from '../service/IssuesService';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import UserApp from './UserApp';
import NavBar from './NavBar';
import { Col,Button, ButtonGroup, ButtonToolbar, Dropdown,DropdownButton,Nav,Navbar,Form,FormControl, Container} from 'react-bootstrap';
import LogoutComponent from './LogoutComponent';


const USERNAME = 'username'
const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};
class CreatePage extends Component {
 dsp=true;
  state = {
    open: false
  };
  constructor(props) {
    super(props);
    this.state = {
      users:[],
      projects: []
    };
  }
  
  onProjectChange = project => {
    this.setState(() => ({
      project
    }));
  };
componentDidMount() {
  this.refreshPermissions();
  this.refreshUsers();
}
refreshPermissions() {
  PermissionDataService.retrieveAllPermissions(USERNAME)//HARDCODED
      .then(
          response => {
            console.log(response.data);
              this.setState({ projects: response.data })
          }
      )
}
refreshUsers() {
  UsersDataService.retrieveAllUsers(USERNAME)//HARDCODED
      .then(
          response => {
            console.log(response.data);
              this.setState({ users: response.data })
          }
      )
}
  handle2 = (e) => {
    
    document.getElementById('frm1').style.display="block";
   
  }
  handleChange = (e) => {
    document.getElementById('frm1').style.display="none";
    var k;
  var id=e.target.value;
   var p=this.state.projects;
       for (var i=0;i<p.length;i++)
       {
          if(id==p[i].projectId) 
            k=p[i].permissionDescription;
    }
  
   
    
    if(k==="READ") {
      this.dsp=true;
      document.getElementById('bt1').style.display="none";
      
      console.log(1);
    }
    else
    {
    this.dsp=false; 
    console.log(2);    
    document.getElementById('bt1').style.display="inline-block";
     
    }
    if(id==="n")
    {
      document.getElementById('bt1').style.display="none";
    }
    }
    handle3=(e)=>{
      e.preventDefault();
      const form = e.target;
     
      const sendtext100 =document.getElementById("prj").value/1;
      const sendtext =document.getElementById("issue").value;
      const sendtext2 =document.getElementById("descr").value;
       var sendtext3 =document.getElementById("ass1").value/1;
      const sendtext4 =document.getElementById("asse").value/1;
      const sendtext5 =document.getElementById("type").value;
      const sendtext6 =document.getElementById("stat1").value/1;
      const sendtext7 =document.getElementById("stat2").value;
    
if(sendtext3=="-1")
{
  for (var i=0;i<this.state.users.length;i++)
  {
    if(this.state.users[i].username==sessionStorage.getItem("authenticatedUser"))
    {
      sendtext3=this.state.users[i].userID;
      break;
    }
  }
}
const textjs={
  "title":sendtext,
  "description":sendtext2,
  "type":sendtext5,
  "otherDetails":sendtext7,
  "projectId":sendtext100,
   "statusId":sendtext6,
   "assigneeId":sendtext4,

};
 
    console.log(JSON.stringify(textjs));
    fetch("http://localhost:8080/issues",
    {
        method: "POST",
        cache: "no-cache",
        headers:{
            "Content-Type": "application/json",
            "Authorization": "Bearer "+sessionStorage.getItem('token')
        },
        body: JSON.stringify(textjs)
    })
    .then(response=> {console.log(response.json());})
      
      
    }
 render() {
  
  const { projects,open,users } = this.state;
   return(
    
     <>
    
    <NavBar/>  
    <div className="Container">
                <Header1/>
                <div className="cont1">
              <div className="row">
        <div className="col-md-3">

        </div>
        <div className="col-md-3">
        <select  onChange={this.handleChange} 
              size="large"
              placeholder="Project"
              style={{ width: '100%',height:'40px', fontSize:'13pt' }}
              id="prj"
              >
          <option value="n" >Select project</option><hr></hr>
      {this.state.projects.map(
      project => 
      <option value={project.projectId} >
      {project.projectName}
      </option>
      )}
    </select>  &nbsp;&nbsp;&nbsp;&nbsp;
    </div>
     
    <div className="col-md-3">
    <Button id="bt1" onClick={this.handle2} 
                size="xlarge"
                type="success"
               // onClick={this.handleChange}
               style={{display:"none"}}
              >
                Create Issue
              </Button>
        </div>
    </div>
      
       <div id="frm1" style={{display:"none"}}>
       <Form onSubmit={this.handle3}>
       <Form.Row>
      <Form.Group as={Col} controlId="formGridEmail">
        <Form.Label>Issue Title </Form.Label>
        <Form.Control type="text" required placeholder="New issue" id="issue" style={{ width: '100%',height:'40px', fontSize:'13pt' }} />
      </Form.Group>
      <Form.Group as={Col} controlId="formGridPassword">
        <Form.Label>Description</Form.Label>
        <Form.Control type="text" required placeholder="Description" id="descr" style={{ width: '100%',height:'40px', fontSize:'13pt' }} />
      </Form.Group>
    </Form.Row>

    <div className="row">
     
        <div className="col-md-4">
    <Form.Group as={Col} controlId="formGridState" >
        <Form.Label style={{  fontSize:'8pt' }}>Assignor &nbsp;</Form.Label>
        <select onChange={this.handleChange}   size="large" id="ass1" style={{ width: '100%',height:'40px', fontSize:'13pt' }}
            
              onChange={this.onUserChange}
            
              >
    <option value="-1">{sessionStorage.getItem("authenticatedUser")}</option>
    {/* {this.state.users.map(
      user => 
      <option value={user.userID}>
      {user.username}  --{user.userID}
      </option>
      )} */}
    </select> 
    </Form.Group>
    </div>
    <div className="col-md-4">
      <Form.Group as={Col} controlId="formGridState" >
      <Form.Label style={{  fontSize:'8pt' }}>Assignee&nbsp;</Form.Label>
      <select onChange={this.handleChange}  size="large" id="asse"  style={{ width: '100%',height:'40px', fontSize:'13pt' }}
              placeholder="User"
              // style={{ width: '50%' }}
              onChange={this.onUserChange}
              //value={users}
              >
    {this.state.users.map(
      user => 
      <option value={user.userID}>
      {user.username}
      </option>
      )}
    </select>
        
      </Form.Group>
     
    </div>
    <div className="col-md-4">
      <Form.Group as={Col} controlId="formGridCity">
        <Form.Label>Type</Form.Label>
        <Form.Control as="select" id="type" required  style={{ width: '100%',height:'40px', fontSize:'13pt' }}>
                    <option value="">Enter project type</option>
                    <option>ERROR</option>
                    <option>IMPROVEMENT</option>
                    <option>OTHER</option>
                    </Form.Control>
      </Form.Group>
      </div>
      </div>
      <div className="row">
      <div className="col-md-6">
      <Form.Group   controlId="formGridState">
        <Form.Label>Status</Form.Label>
        <Form.Control as="select" id="stat1"  required  style={{ width: '100%',height:'40px', fontSize:'13pt' }}>
                    <option value="">Enter project status</option>
                    <option value="1">OPEN</option>
                    <option value="2">RESOLVED</option>
                    <option value="3">REOPEN</option>
                    </Form.Control>
    
      </Form.Group>
      </div>
        
  <div className="col-md-6">
      <Form.Group as={Col} controlId="formGriddet"  >
        <Form.Label>Other details</Form.Label>
        <Form.Control type="text"  placeholder="Other details" id="stat2"  style={{ width: '100%',height:'40px', fontSize:'13pt' }} />
      </Form.Group>
</div>
    </div>
    

   <div className="row">
   

     <div className="col-md-3">
    <Button variant="primary" type="submit" id="bt1">
      Create
    </Button>&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div className="col-md-3"></div>
    <Button onClick={() => window.location.reload(false)} id="bt1">Clear</Button>
    </div>
 
    
  </Form> 
    </div>
    </div>
    </div>
  
     </>
   );
                   
 }
}
export default CreatePage;