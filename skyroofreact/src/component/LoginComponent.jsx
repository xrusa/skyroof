import React, { Component } from 'react';
import AuthenticationService from '../service/AuthenticationService';
import Header1 from "../Header1";

class LoginComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]
                    : event.target.value
            }
        );
    }

    loginClicked() {
        //in28minutes,dummy
        // if(this.state.username==='in28minutes' && this.state.password==='dummy'){
        //     AuthenticationService.registerSuccessfulLogin(this.state.username,this.state.password)
        //     this.props.history.push(`/courses`)
        //     //this.setState({showSuccessMessage:true})
        //     //this.setState({hasLoginFailed:false})
        // }
        // else {
        //     this.setState({showSuccessMessage:false})
        //     this.setState({hasLoginFailed:true})
        // }

        // AuthenticationService
        //     .executeBasicAuthenticationService(this.state.username, this.state.password)
        //     .then(() => {
        //         AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password)
        //         this.props.history.push('/counter')
        //     }).catch(() => {
        //         this.setState({ showSuccessMessage: false })
        //         this.setState({ hasLoginFailed: true })
        //     })

        AuthenticationService
            .executeJwtAuthenticationService(this.state.username, this.state.password)
            .then((response) => {
                AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token)
                localStorage.setItem('USER_NAME_SESSION_TOKEN',response.data.token)
                console.log("hello");
                console.log(response.data.token)
                // dispatchEvent(this.state.username(data.user))
                this.props.history.push(`/home`)
            }).catch(() => {
                this.setState({ showSuccessMessage: false })
                this.setState({ hasLoginFailed: true })
            })

    }

    render() {
        return (
                <div className="container-fluid">
                    <div className="row">
                        <Header1/>
                    </div>
                         <div className="row">
                             <div className="col-md-3">

                        </div>
                        <div className="col-md-6 class1">

                            <h1>Login</h1><hr></hr>

                    {/*<ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed}/>*/}
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                     {this.state.showSuccessMessage && <div>Login Sucessful</div>}
                    {/*<ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage}/>*/}
                    <p>Username: <input type="text" name="username" value={this.state.username} onChange={this.handleChange} /></p>
                            <p> Password: <input type="password" name="password" value={this.state.password} onChange={this.handleChange} /></p><hr></hr>
                            <button className="btn btn-default" onClick={this.loginClicked}>Login</button>    &nbsp;&nbsp;
                            <button className="btn btn-default" onClick={() => window.location.reload(false)}>Clear</button><hr></hr>
                        </div>
                        <div className="col-md-3">
                        </div>
                         </div>
                </div>
        )
    }
}

export default LoginComponent