import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import AuthenticationService from '../service/AuthenticationService';
import LogoutComponent from './LogoutComponent';

class MenuComponent extends Component {

    render() {
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();

        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div><a href="https://www.intrasoft-intl.com/" className="navbar-brand">MySkyroof</a></div>
                    <ul className="nav navbar-nav">
            <li className="dropdown">
            <a className="dropdown-toggle" data-toggle="dropdown" href="#">Issue Management
                <span className="caret"></span></a>
            <ul className="dropdown-menu">
                <li><a href="createpage">Create</a></li>
                <li className="divider"></li>
                <li><a href="#">Search</a></li>
            </ul>
        </li>
        </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end"><span className="glyphicon glyphicon-log-out"></span> 
                        {!isUserLoggedIn && <li><Link className="nav-link" to="/login">Login</Link></li> }
                        {isUserLoggedIn && <li><Link className="nav-link" to="/logout" onClick={AuthenticationService.logout}>Logout</Link></li>}
                    </ul>
                </nav>
            </header>
        )
    }
}

export default withRouter(MenuComponent)