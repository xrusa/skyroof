import axios from 'axios'
const API_URL = 'http://localhost:8080'
const PERMISSION_API_URL = `${API_URL}/projectsper`

class PermissionDataService {

    retrieveAllPermissions(name) {
        //console.log('executed service')
        let token = sessionStorage.getItem('token')
        
        return axios.get(`${PERMISSION_API_URL}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token 
                }
            }
        );
    }
}

export default new PermissionDataService()
