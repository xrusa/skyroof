import axios from 'axios'
import { queryString } from '../component/Home.jsx'

const API_URL = 'http://localhost:8080'
const ISSUES_API_URL = `${API_URL}/issues?`
class IssueDataService {

    retrieveAllIssues(name,params) {
        //console.log('executed service')
        console.log('params')
        console.log('name')
    
        
        let token = localStorage.getItem('USER_NAME_SESSION_TOKEN');
    
        
        return axios.get(`${ISSUES_API_URL}${queryString}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token 
                }
            }
        );
    }
}

export default new IssueDataService()
